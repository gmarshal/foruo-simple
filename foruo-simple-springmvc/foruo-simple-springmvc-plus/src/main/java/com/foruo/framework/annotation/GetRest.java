package com.foruo.framework.annotation;

import java.lang.annotation.*;

/**
 * GetRest
 * 表示是get请求，并且是Rest方式
 * @author GaoYuan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface GetRest {

	String value() default "";

}