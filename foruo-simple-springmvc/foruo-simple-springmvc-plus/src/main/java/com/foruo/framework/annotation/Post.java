package com.foruo.framework.annotation;

import java.lang.annotation.*;

/**
 * Post
 * 表示是post请求
 * @author GaoYuan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Post {

	String value() default "";

}