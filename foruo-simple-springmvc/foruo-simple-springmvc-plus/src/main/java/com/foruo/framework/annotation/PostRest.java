package com.foruo.framework.annotation;

import java.lang.annotation.*;

/**
 * PostRest
 * 表示是post请求，并且是Rest方式
 * @author GaoYuan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface PostRest {

	String value() default "";

}