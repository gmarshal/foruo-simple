package com.foruo.framework.annotation;

import java.lang.annotation.*;

/**
 * Controller
 * 自定义的注解，类似Spring的@Controller
 * @author GaoYuan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface Controller {

	String value() default "";

}