package com.foruo.framework.annotation;

import java.lang.annotation.*;

/**
 * Get
 * 表示是get请求
 * @author GaoYuan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Get {

	String value() default "";

}