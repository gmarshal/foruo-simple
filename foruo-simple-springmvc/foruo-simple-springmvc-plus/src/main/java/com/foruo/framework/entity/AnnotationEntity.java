package com.foruo.framework.entity;

import java.lang.reflect.Method;

/**
 * 只是一个对象而已，用来封装class与method的对应关系
 * @author GaoYuan
 * @date 2018/5/10 下午10:03
 */
public class AnnotationEntity {

    private Class<?> controller;
    private Method method;

    public AnnotationEntity(Class<?> controller,Method method){
        this.controller = controller;
        this.method = method;
    }

    public AnnotationEntity(){}


    public Class<?> getController() {
        return controller;
    }

    public void setController(Class<?> controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }
}
