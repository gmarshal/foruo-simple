package com.foruo.framework;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.foruo.framework.annotation.Get;
import com.foruo.framework.annotation.GetRest;
import com.foruo.framework.annotation.Post;
import com.foruo.framework.annotation.PostRest;
import com.foruo.framework.entity.AnnotationEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * 调度器
 * 功能类似 Spring的DispatcherServlet
 *
 * 尚未优化
 *
 * @author GaoYuan
 * @date 2018/5/10 下午3:24
 */
public class MyDispatcherServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //请求URI
        String uri = request.getRequestURI();
        //项目路径
        String projectPath = request.getContextPath();
        if(projectPath!=null && !"".equals(projectPath)){
            //如果有项目路径，则将uri中的项目路径移除
            uri = uri.replaceFirst(projectPath,"");
        }
        //获取缓存的方法Map对象
        Map<String,Map<String,AnnotationEntity>> methodMap = MethodMapInstance.getMethodMap();
        Map<String,AnnotationEntity> getMap;
        AnnotationEntity entity;
        getMap = methodMap.get(Get.class.getName());
        entity = getMap.get(uri);

        if(!getMap.isEmpty() && entity!=null){
            /** page */
            // 设置响应内容类型
            response.setCharacterEncoding("UTF-8");
            //执行返回
            try {
                String resultPage = (String)entity.getMethod().invoke(entity.getController().newInstance(), request, response);
                RequestDispatcher rd = request.getRequestDispatcher(resultPage + ".jsp");
                rd.forward(request, response);
            }catch (Exception e){
                e.printStackTrace();
            }

        }else{
            /** rest */
            // 设置响应内容类型
            response.setContentType("application/json;charset=utf-8");
            response.setCharacterEncoding("UTF-8");
            getMap = methodMap.get(GetRest.class.getName());
            entity = getMap.get(uri);
            if(entity != null){

                //执行返回
                try {
                    Object object = entity.getMethod().invoke(entity.getController().newInstance(), request, response);
                    PrintWriter out = response.getWriter();
                    JSONObject jsonObject = (JSONObject) JSON.toJSON(object);
                    System.out.println(jsonObject);
                    out.println(jsonObject);
                    out.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /** 类似get */
        //请求URI
        String uri = request.getRequestURI();
        //项目路径
        String projectPath = request.getContextPath();
        if(projectPath!=null && !"".equals(projectPath)){
            //如果有项目路径，则将uri中的项目路径移除
            uri = uri.replaceFirst(projectPath,"");
        }
        //获取缓存的方法Map对象
        Map<String,Map<String,AnnotationEntity>> methodMap = MethodMapInstance.getMethodMap();
        Map<String,AnnotationEntity> getMap;
        AnnotationEntity entity;
        getMap = methodMap.get(Post.class.getName());
        entity = getMap.get(uri);

        if(!getMap.isEmpty() && entity!=null){
            /** page */
            // 设置响应内容类型
            response.setCharacterEncoding("UTF-8");
            //执行返回
            try {
                String resultPage = (String)entity.getMethod().invoke(entity.getController().newInstance(), request, response);
                RequestDispatcher rd = request.getRequestDispatcher(resultPage + ".jsp");
                rd.forward(request, response);
            }catch (Exception e){
                e.printStackTrace();
            }

        }else{
            /** rest */
            // 设置响应内容类型
            response.setContentType("application/json;charset=utf-8");
            response.setCharacterEncoding("UTF-8");
            getMap = methodMap.get(PostRest.class.getName());
            entity = getMap.get(uri);
            if(entity != null){

                //执行返回
                try {
                    Object object = entity.getMethod().invoke(entity.getController().newInstance(), request, response);
                    PrintWriter out = response.getWriter();
                    JSONObject jsonObject = (JSONObject) JSON.toJSON(object);
                    System.out.println(jsonObject);
                    out.println(jsonObject);
                    out.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

}
