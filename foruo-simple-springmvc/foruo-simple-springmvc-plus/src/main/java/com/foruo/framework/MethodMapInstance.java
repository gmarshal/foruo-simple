package com.foruo.framework;

import com.foruo.framework.annotation.Controller;
import com.foruo.framework.entity.AnnotationEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 初始化所有controller方法对象
 * 类似Spring的Bean初始化
 * @author GaoYuan
 * @date 2018/5/10 下午6:01
 */
public class MethodMapInstance {

    private static Map<String,Map<String,AnnotationEntity>> methodMap = new HashMap<>();

    public static Map<String, Map<String, AnnotationEntity>> getMethodMap() {
        if(methodMap==null || methodMap.isEmpty()){
            List<Class<?>> list = AnnotationUtils.getPackageController("com.foruo.modules",Controller.class);
            AnnotationUtils.setMethodMap(list,methodMap);
        }
        return methodMap;
    }



}
