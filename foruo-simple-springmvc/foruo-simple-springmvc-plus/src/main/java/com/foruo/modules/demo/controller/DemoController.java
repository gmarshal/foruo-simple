package com.foruo.modules.demo.controller;

import com.foruo.framework.annotation.*;
import com.foruo.modules.demo.entity.DemoEntity;
import com.foruo.modules.demo.service.DemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 仿造 controller 案例
 * @author GaoYuan
 * @date 2018/5/10 下午5:58
 *
 * 注意： 这边的 @Controller 并不是 spring 的，而是自定义的一个注解
 */
@Controller
public class DemoController {

    DemoService demoService = new DemoService();


    /**
     * 跳转对应页面【get】
     * @author GaoYuan
     * @date 2018/5/11 上午8:59
     */
    @Get("/toPage")
    public String toPage(HttpServletRequest request, HttpServletResponse response){
        try {
            String id = request.getParameter("id");
            DemoEntity demoEntity = demoService.get(id);
            request.setAttribute("demoEntity",demoEntity);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "demo";
    }
    /**
     * 获取json对象【get】
     * @author GaoYuan
     * @date 2018/5/11 上午8:59
     */
    @GetRest("/toJson")
    public DemoEntity toJson(HttpServletRequest request, HttpServletResponse response){
        try {
            String id = request.getParameter("id");
            DemoEntity demoEntity = demoService.get(id);
            return demoEntity;
        }catch (Exception e){
            e.printStackTrace();
        }
        return new DemoEntity();
    }
    /**
     * 跳转对应页面【post】
     * @author GaoYuan
     * @date 2018/5/11 上午8:59
     */
    @Post("/toPagePost")
    public String toPagePost(HttpServletRequest request, HttpServletResponse response){
        try {
            String id = request.getParameter("id");
            DemoEntity demoEntity = demoService.get(id);
            demoEntity.setName(demoEntity.getName()+"（我是post提交的）");
            request.setAttribute("demoEntity",demoEntity);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "demo";
    }
    /**
     * 获取json对象【post】
     * @author GaoYuan
     * @date 2018/5/11 上午8:59
     */
    @PostRest("/toJsonPost")
    public DemoEntity toJsonPost(HttpServletRequest request, HttpServletResponse response){
        try {
            String id = request.getParameter("id");
            DemoEntity demoEntity = demoService.get(id);
            demoEntity.setName(demoEntity.getName()+"（我是post提交的）");
            return demoEntity;
        }catch (Exception e){
            e.printStackTrace();
        }
        return new DemoEntity();
    }

}
