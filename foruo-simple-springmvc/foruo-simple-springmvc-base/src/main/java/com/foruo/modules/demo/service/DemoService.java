package com.foruo.modules.demo.service;

import com.foruo.modules.demo.ConnectionFactory;
import com.foruo.modules.demo.dao.DemoDao;
import com.foruo.modules.demo.entity.DemoEntity;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 模拟Service层
 * @author GaoYuan
 * @date 2018/5/10 下午9:31
 */
public class DemoService {

    /** 引用dao */
    DemoDao demoDao = new DemoDao();

    /**
     * 查询详情
     * @author GaoYuan
     * @date 2018/5/10 下午9:31
     */
    public DemoEntity get(String id) throws SQLException{
        DemoEntity demoEntity = null;
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        Connection connection = connectionFactory.getConnection();
        try {
            connection.setAutoCommit(false);
            demoEntity = demoDao.get(connection,id);
            connection.commit();
        }catch (SQLException e){
            e.printStackTrace();
            connection.rollback();
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        return demoEntity;
    }
}
