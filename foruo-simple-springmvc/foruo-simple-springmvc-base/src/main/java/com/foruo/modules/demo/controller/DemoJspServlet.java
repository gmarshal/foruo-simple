package com.foruo.modules.demo.controller;

import com.foruo.modules.demo.entity.DemoEntity;
import com.foruo.modules.demo.service.DemoService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 模拟controller
 * 跳转页面
 * @author GaoYuan
 * @date 2018/5/10 下午3:24
 */
public class DemoJspServlet extends HttpServlet{

    DemoService demoService = new DemoService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置响应内容类型
        response.setCharacterEncoding("UTF-8");
        try {
            String id = request.getParameter("id");
            DemoEntity demoEntity = demoService.get(id);
            request.setAttribute("demoEntity",demoEntity);
            RequestDispatcher rd = request.getRequestDispatcher("demo" + ".jsp");
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }

}
