package com.foruo.modules.demo.dao;

import com.foruo.modules.demo.entity.DemoEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 模拟dao层
 * @author GaoYuan
 * @date 2018/5/10 下午9:30
 */
public class DemoDao {

    /**
     * 查询demo详情
     * @author GaoYuan
     * @date 2018/5/10 下午9:30
     */
    public DemoEntity get(Connection connection, String id) throws SQLException{
        String sql = "select * from demo where id = ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        DemoEntity demoEntity = new DemoEntity();
        while (rs.next()) {
            demoEntity.setId(id);
            demoEntity.setName(rs.getString("name"));
        }
        return demoEntity;
    }
}
