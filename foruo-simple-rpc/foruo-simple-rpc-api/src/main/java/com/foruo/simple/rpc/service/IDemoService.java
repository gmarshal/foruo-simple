package com.foruo.simple.rpc.service;

import com.foruo.simple.rpc.entity.DemoEntity;

public interface IDemoService {

    /**
     * 获取demo对象
     * @author GaoYuan
     */
    DemoEntity getDemo(String id);

}
