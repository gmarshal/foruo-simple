package com.foruo.simple.rpc.entity;

import java.io.Serializable;

/**
 * 测试对象
 * @author GaoYuan
 */
public class DemoEntity implements Serializable{

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
