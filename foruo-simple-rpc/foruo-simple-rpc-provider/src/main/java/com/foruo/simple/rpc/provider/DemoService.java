package com.foruo.simple.rpc.provider;

import com.foruo.simple.rpc.entity.DemoEntity;
import com.foruo.simple.rpc.service.IDemoService;

/**
 * 接口具体实现
 * @author GaoYuan
 * @date 2018/9/26 下午8:00
 */
public class DemoService implements IDemoService {

    @Override
    public DemoEntity getDemo(String id) {
        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setId("1");
        demoEntity.setName("gaoyuan");
        return demoEntity;
    }
}
